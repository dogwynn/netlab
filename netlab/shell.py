from pathlib import Path
import logging

import click

from .network import write_network_files

log = logging.getLogger('shell')
log.addHandler(logging.NullHandler())

def setup_logging(loglevel):
    level = logging.getLevelName(loglevel.upper())
    fmt = (
        '{asctime} {levelname: <8} [{name}]:{lineno: >4}: {message}'
    )
    datefmt = '%Y-%m-%d %H:%M:%S'
    logging.basicConfig(level=level, datefmt=datefmt, format=fmt, style='{')

class PathParam(click.Path):
    def coerce_path_result(self, rv):
        return Path(super().coerce_path_result(rv))

@click.command()
@click.argument('seed')
@click.option('-c', '--config', default='netlab.yml',
              type=PathParam(exists=True, resolve_path=True),
              help=('YAML config file path (default: netlab.yml)'))
# @click.option('-m', '--module',
#               type=PathParam(exists=True, resolve_path=True),
#               default='generate.py')
# @click.option('-c', '--context',
#               type=PathParam(exists=True, dir_okay=True, resolve_path=True),
#               default='.')
# @click.option('-d', '--dockerfile',
#               help=('Dockerfile for network nodes. If not given,'
#                     ' will be generated.'),
#               type=PathParam(exists=True, resolve_path=True))
# @click.option('-l', '--lab',
#               help=('Lab content directory. Defaults to "lab" directory'
#                     ' in context directory'),
#               type=PathParam(exists=True, dir_okay=True, resolve_path=True))
# @click.option('--force', is_flag=True)
@click.option('--loglevel', default='info')
def main(seed, config, loglevel):
    setup_logging(loglevel)

    write_network_files(seed, config)
