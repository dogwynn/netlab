import importlib
from pathlib import Path
from itertools import starmap as _starmap

from toolz import curry, pipe
from toolz.curried import (
    assoc as _assoc, assoc_in as _assoc_in,
    flip, filter, map, do, merge,
)
from multipledispatch import dispatch
import ruamel.yaml

@curry
def assoc(k, vfunc, d, *a, **kw):
    return _assoc(d, k, vfunc(d), *a, **kw)

@curry
def assoc_in(keys, vfunc, d, *a, **kw):
    return _assoc_in(d, keys, vfunc(d), *a, **kw)

@curry
def setdefault(k, vfunc, d):
    return d if k in d else assoc(k, vfunc, d)

@curry
def update(func, d):
    new = d.copy()
    new.update(func(d))
    return new

def catset(iterables):
    new = set()
    for v in iterables:
        new.update(v)
    return new

starmap = curry(_starmap)

@curry
def var(func, iterable):
    return func(*iterable)

@curry
def mapset(func, iterable):
    new = set()
    for v in iterable:
        new.update(func(v))
    return new

def yaml_dump(data):
    return ruamel.yaml.dump(data, Dumper=ruamel.yaml.RoundTripDumper,
                            default_flow_style=False)

def yaml_load(path):
    return ruamel.yaml.safe_load(Path(path).read_text())
                            

@dispatch(str)
def module_from_path(path):
    return module_from_path(Path(path))
@dispatch(Path)
def module_from_path(path):
    spec = importlib.util.spec_from_file_location(
        path.stem, path.expanduser().resolve()
    )
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module
