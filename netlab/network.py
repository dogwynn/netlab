#!/usr/bin/env python3
import subprocess
import logging
import pprint
from pathlib import Path
import platform

import networkx as nx

from .common import (
    module_from_path,
    yaml_dump, yaml_load,
    curry, pipe, map, starmap, catset, mapset, filter, do,
    update, assoc, assoc_in, setdefault,
)

from .templates import (
    start_template, stop_template, connect_template,
    start_ps1_template, stop_ps1_template, connect_ps1_template,
    dockerfile_template, supervisor_template,
)

log = logging.getLogger('network')
log.addHandler(logging.NullHandler())

def get_routers(G: nx.Graph):
    return {n for n in G if G.node.get('router')}

def generate_network(G: nx.Graph, config: dict):
    N = nx.from_edgelist(G.edges())

    routers = get_routers(G)
    log.info(f'routers: {routers}')
    non_routers = {n for n in G if n not in routers}
    log.info(f'non-routers: {non_routers}')

    without_routers = N.subgraph(non_routers)
    
    for i, comp in enumerate(nx.connected_components(without_routers)):
        log.info(comp)
        for n in comp:
            N.node[n].update(service_node(config))
            N.node[n]['networks'] = [f'net{i:02}']
            N.node[n]['hostname'] = f'node{n:02}'

    for i, rnode in enumerate(routers):
        N.node['hostname'] = 'router{i:02}'
        N.node[rnode]['networks'] = pipe(
            G.neighbors(rnode),
            filter(lambda n: n in non_routers),
            mapset(lambda n: N.node[n]['networks']),
            sorted,
        )

    log.info(sorted(N))

    return N

def service_node(config: dict):
    return {
        'build': {
            'context': str(config['context']),
            'dockerfile': config['dockerfile'].name,
        },
        'image': config['image'],
        'command': '/usr/sbin/sshd -D',
        'environment': {
            'PYTHONUNBUFFERED': 'true',
        },
        'volumes': [f'{config["lab"]}:/lab'],
        'networks': [],
    }

def default_compose():
    return {
        'version': '2',
        'services': {},
        'networks': {},
    }

def get_docker_compose(N: nx.Graph):
    return pipe(
        default_compose(),
        update(lambda d: {
            'services': {N.node[n]['hostname']: N.node[n] for n in sorted(N)},
            'networks': pipe(
                (N.node[n]['networks'] for n in N),
                catset,
                sorted,
                lambda ns: {n: None for n in ns},
            ),
        }),
    )

def load_config(seed: str, path: Path):
    config = pipe(
        {'expose_ssh': False,
         'python': {'version': '3.7',
                    'requirements': []},
         'context': path.parent,
         'module': 'generate.py',
         'dockerfile': f'.Dockerfile-{seed}',
         'supervisor': f'.supervisord-{seed}.conf',
         'programs': [],
         'apt': [],
         'lab': 'lab'},
        update(lambda d: yaml_load(path)),
        assoc('context', lambda d: Path(d['context']).resolve()),
        assoc('module', lambda d: Path(d['context'], d['module'])),
        assoc('dockerfile', lambda d: Path(d['context'], d['dockerfile'])),
        assoc('lab', lambda d: Path(d['context'], d['lab'])),
        assoc('supervisor', lambda d: Path(d['context'], d['supervisor'])),
        assoc('path', lambda d: Path(path).expanduser().resolve()),
    )

    return config

def mtime(p):
    return p.stat().st_mtime

def needs_rebuild(path, *predicate_paths):
    if path.exists():
        if all(mtime(path) > mtime(p) for p in predicate_paths):
            return False
    return True

def python_requirements_path(seed: str, config: dict):
    return Path(config['context'], f'.requirements-{seed}.txt')

def write_python_files(seed: str, config: dict):
    reqs = config['python'].get('requirements', [])
    req_path = python_requirements_path(seed, config)
    if reqs:
        config['python']['requirements_path'] = req_path
        req_path.write_text('\n'.join(reqs))

def write_supervisor_conf(seed: str, config: dict):
    config['supervisor'].write_text(
        supervisor_template().render(config=config)
    )
    
def write_dockerfile(seed: str, config: dict):
    config['dockerfile'].write_text(
        dockerfile_template().render(config=config)
    )

def get_net_templates(config):
    system = platform.system()
    if system == 'Windows':
        return [
            (Path(config['context'], 'start.ps1'), start_ps1_template()),
            (Path(config['context'], 'stop.ps1'), stop_ps1_template()),
            (Path(config['context'], 'connect.ps1'), connect_ps1_template())
        ]
    return [
        (Path(config['context'], 'start'), start_template()),
        (Path(config['context'], 'stop'), stop_template()),
        (Path(config['context'], 'connect'), connect_template()),
    ]
    
def write_network_files(seed: str, config_path: Path):
    '''Generates all necessary scripts and configurations to build a
    Docker network environment

    Args:
      seed (str): Seed value for random network graph

      config_path (Path): Path to netlab YAML configuration file

    Returns: None

    '''
    config = load_config(seed, config_path)

    for func in [write_supervisor_conf, write_python_files,
                 write_dockerfile]:
        func(seed, config)

    # Generate docker-compose.yml file path
    compose_path = Path(config['context'], f'.docker-compose-{seed}.yml')

    # Get random graph generator function from module
    generator = getattr(module_from_path(config['module']), 'generator')

    pipe(
        generate_network(generator(seed), config),
        get_docker_compose,
        yaml_dump,
        compose_path.write_text,
    )

    pipe(
        get_net_templates(config),
        do(print),
        starmap(
            lambda p, t: [
                log.info(p),
                p.write_text(t.render(compose_path=compose_path)),
                p
            ][-1]
        ),
        map(lambda p: p.chmod(0o775)),
        tuple,                  # to consume
    )
