import random

import networkx as nx

from .common import (
    curry, pipe,
)

@curry
def caveman_with_hub(l, k, seed):
    return pipe(
        nx.caveman_graph(l, k),
        lambda g: (len(g), g.nodes(), list(g.edges())),
        lambda g: (g[0], g[2] + [(g[0], n) for n in g[1]]),
        lambda g: (g[0], nx.from_edgelist(g[1]))
    )

@curry
def relaxed_caveman_graph(l, k, p, seed):
    return nx.relaxed_caveman_graph(5, 5, 0.05, random.Random(seed))
