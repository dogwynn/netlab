from pathlib import Path

import jinja2
from pkg_resources import resource_filename

from ..common import pipe

def get_path(path):
    return Path(resource_filename(__name__, path)).resolve()

def add_filters(env: jinja2.Environment):
    return env

def add_functions(env: jinja2.Environment):
    return env

def get_environment():
    return pipe(
        jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(get_path('.'))),
        ),
        add_filters,
        add_functions,
    )

def start_template():
    return get_environment().get_template('start.j2')

def stop_template():
    return get_environment().get_template('stop.j2')

def connect_template():
    return get_environment().get_template('connect.j2')

def start_ps1_template():
    return get_environment().get_template('start.j2')

def stop_ps1_template():
    return get_environment().get_template('stop.j2')

def connect_ps1_template():
    return get_environment().get_template('connect.j2')

def dockerfile_template():
    return get_environment().get_template('Dockerfile.j2')

def supervisor_template():
    return get_environment().get_template('supervisord.conf.j2')
