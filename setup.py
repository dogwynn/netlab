import os
from pathlib import Path

from setuptools import setup, find_packages

HERE = Path(__file__).resolve().parent
def here(*parts):
    return Path(HERE, *parts)

def version():
    return here('VERSION').read_text()

def long_description():
    return here('README.md').read_text()

def all_ext(path, ext, ignore=None):
    ignore = set(ignore) if ignore else {'__pycache__'}
    for root, dirs, files in os.walk(path):
        if Path(root).name not in ignore:
            if any(n.endswith(f'{ext}') for n in files):
                yield str(Path(root, f'*{ext}'))

def catset(*iterables):
    new = set()
    for v in iterables:
        new.update(v)
    return new

def requirements():
    return here('requirements.txt').read_text().strip().splitlines()

setup(
    name='networking_labs',
    packages=find_packages(
        exclude=['tests'],
    ),
    package_dir={
        'netlab': 'netlab',
    },
    package_data={
        'netlab': catset(
            all_ext(here('netlab/templates'), '.j2'),
            all_ext(here('netlab/templates'), '.css'),
            all_ext(here('netlab/templates'), '.js'),
            all_ext(here('netlab/templates'), '.png'),
            all_ext(here('netlab/templates'), '.jpg'),
            all_ext(here('netlab/templates'), '.gif'),
        ),
    },
    include_package_data=True,

    install_requires=requirements(),

    version=version(),
    description='Network generation library using Docker toolset',
    long_description=long_description(),

    url='https://bitbucket.org/dogwynn/netlab',

    author="David O'Gwynn",
    author_email='dogwynn@belhaven.edu',

    license='BSD',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],

    zip_safe=False,

    keywords=('networking generation simulation docker exercises'),

    entry_points={
        'console_scripts': [
            'netlab=netlab.shell:main'
        ],
    },
)
