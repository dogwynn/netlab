# netlab

##### Networking lab generation using Docker toolset

*netlab* is a tool for building simple randomized networks of Docker
containers for analysis and study. Its original purpose was to create
individualized labs for my networking classes.

## Getting Started

These instructions will get *netlab* running running on your local
machine. Once installed you will have the `netlab` command available
in your shell. See Usage below for instructions on how to use the
command.

### Prerequisites

*netlab* has two primary dependencies:

- Python 3.6+
- Docker

#### Docker on Windows

To install Docker on Windows, the best solution is
[Docker for Windows](https://docs.docker.com/docker-for-windows/install/)
but be aware of the system requirements. If your system does not meet
these requirements, you can install
[Docker Toolbox](https://docs.docker.com/toolbox/overview/) instead.

#### Docker on macOS

To install Docker on macOS, the best solution is
[Docker for Mac](https://docs.docker.com/docker-for-mac/install/) but
be aware of the system requirements. If your system does not meet
these requirements, you can install
[Docker Toolbox](https://docs.docker.com/toolbox/overview/) instead.

#### Docker Toolbox

If you have installed Docker Toolbox, you will need to run *netlab*
from the provided preconfigured shell, Docker Quickstart Terminal.

### Installing

Using `pip`:

```
pip install https://bitbucket.org/dogwynn/netlab/src/master
```

Installing from source:

```
git clone https://bitbucket.org/dogwynn/netlab.git
cd netlab
python setup.py install
```

## Usage

To create a lab network using *netlab*, you will need to create a few
files and then run the `netlab` command with a seed value. Usually,
you will create a directory for each of your labs and provide the
required files separately in each directory.

### netlab.yml

The primary configuration file for *netlab* is `netlab.yml`. It is a
YAML-formatted file that looks something like the following:

```yaml
# context: The directory where the relevant files, directories, and
#   configurations for the network lab are located. If not set, this
#   will be the parent directory of this "netlab.yml" file.
# 
# context: "/some/absolute/path"
# 
#  or
# 
# context: "some/relative/path/to/where/netlab/is/called"

# module: Specifies the Python module file name that contains the
#   required "generator" function. If not set, then netlab will look
#   for a file called "generate.py" in the same directory as
#   "netlab.yml"
# 
# module: my_generator_module.py

# dockerfile: The name of a bespoke Dockerfile for the indiviual hosts
#   in the network. If this is set, then the specified Dockerfile is
#   responsible for providing whatever file-system connectivity is
#   needed. It also will invalidate most of the rest of the
#   configuration options.
# 
# dockerfile: MyDockerfile

# apt: Which apt packages should be installed in the default
#   container? The openssh-server package is the only one installed by
#   default. Ignored if "dockerfile" option is set.
# 
apt:
  - bash-completion
  - net-tools
  - iproute2
  - iputils-ping
  - nmap
  - tcpdump
  - tshark
  - fping
  - bind9-host

# python: Python settings for the default container
# 
python:
  # version: Which Python version for the default container. This
  #   determines which python-slim image the container will be built
  #   from.
  # 
  version: 3.7

  # requirements: Which Python packages should be installed in the
  #   container?
  # 
  requirements:
    - networkx
    - scapy

# lab: Name of the sub-directory which contains the lab material
#   (READMEs, study drills, etc.). This will be the working directory
#   of the container, i.e. it will be the directory that the student
#   will be in when they use the "connect" script to join a host in
#   the network. Ignored if "dockerfile" option is set.
# 
lab: lab

# expose_ssh: Should the SSH port be exposed on the default Docker
#   container?  If you plan on SSH'ing into the container (as opposed
#   to using the "connect" script), then this should be set to "yes"
#   or "true". Ignored if "dockerfile" option is set.
# 
expose_ssh: no
```

### generator(seed: str)

You need to provide a Python module in your lab directory that
provides a specified function, `generator(seed: str)` to generate a
(potentially random) NetworkX graph that models your network. By
default, *netlab* will look for a file named `generate.py` to find
this function.

A minimal example:

```python
import networkx as nx

def generator(seed: str):
    graph = nx.Graph()
    graph.add_node(1)
    graph.add_node(2)
    graph.add_edge((1, 2))
    return graph
```

This generates a network with two hosts on a shared network.





